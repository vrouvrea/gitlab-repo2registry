from fabric.api import local

"""
BSD License

Copyright (c) 2018, Inria
All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the Inria nor the names of its contributors may be used
  to endorse or promote products derived from this software without specific
  prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

__author__ = "Vincent Rouvreau"
__copyright__ = "Copyright (C) 2018 Inria"
__license__ = "BSD"

def gitlab_registry_info():
    """This fabric function returns the gitlab registry info when launched
    from a gitlab local clone.
    `fab gitlab_registry_info`

    :rtype: string, string
    :returns: registry, project.
    """
    output = local("git remote show origin", capture=True)
    output_stdout = output.stdout.split("\n")
    for line in output_stdout:
        # Retrieve the Fetch line
        if ("Fetch URL:") in line:
            # line = '  Fetch URL: git@gitlab.com:gitlab_user/gitlab-repo2registry.git'
            registry = "registry." + line[line.find("@")+1:line.rfind(":")]
            project = line[line.rfind(":")+1:line.rfind(".git")]

            print("registry = %s" % registry)
            print("project = %s" % project)
            return registry, project

def gitlab_registry_login(registry):
    """This fabric function logs to a gitlab registry.
    `fab gitlab_registry_login:registry=registry.gitlab.com`

    :param registry: The registry (i.e.: 'registry.gitlab.com').
    :type registry: string
    :rtype: bool
    :returns: Login success information.
    """
    output = local("docker login %s" % (registry))
    return not output.failed

def gitlab_registry_build(registry, project, docker_tag, path):
    """This fabric function builds a Docker image for a gitlab registry.
    `fab gitlab_registry_build:registry=registry.gitlab.com,project=group/project,docker_tag=latest_ubuntu,path=/home/user/Dockerfiles/latest_ubuntu`

    :param registry: The registry (i.e.: 'registry.gitlab.com').
    :type registry: string
    :param project: The project (i.e.: 'group/project').
    :type project: string
    :param docker_tag: The docker_tag (i.e.: 'latest_ubuntu').
    :type docker_tag: string
    :param path: The path (i.e.: '/home/user/Dockerfiles/latest_ubuntu').
    :type path: string
    :rtype: bool
    :returns: Build success information.
    """
    output = local("docker build -t %s/%s/%s:tag %s/%s" % (registry, project, docker_tag, path, docker_tag))
    return not output.failed

def gitlab_registry_push(registry, project, docker_tag):
    """This fabric function pushes a Docker image in a gitlab registry.
    `fab gitlab_registry_push:registry=registry.gitlab.com,project=group/project,docker_tag=latest_ubuntu`

    :param registry: The registry (i.e.: 'registry.gitlab.com').
    :type registry: string
    :param project: The project (i.e.: 'group/project').
    :type project: string
    :param docker_tag: The docker_tag (i.e.: 'latest_ubuntu').
    :type docker_tag: string
    :rtype: bool
    :returns: Push success information.
    """
    output = local("docker push %s/%s/%s:tag" % (registry, project, docker_tag))
    return not output.failed

def list_docker_tags(path="."):
    """This fabric function lists Docker tags from a path.
    `fab list_docker_tags:path=/home/user/Dockerfiles/latest_ubuntu`

    :param registry: The path (i.e.: '/home/user/Dockerfiles/latest_ubuntu').
    :type registry: string
    :rtype: list of string
    :returns: Returns a list of 'path/*/Dockerfile'.
    """
    output = local("ls %s/*/Dockerfile" % (path), capture=True)
    output_stdout = output.stdout.split("\n")
    docker_tags = []
    for dockerfile in output_stdout:
        image_name = dockerfile.split("/")
        docker_tags.append(image_name[-2])
    print("docker_tags = %s" % docker_tags)
    return docker_tags

def repo2registry(registry="", project="", path="."):
    """This fabric function builds and push Docker images from all '*/Dockerfile'.
    `fab repo2registry:registry=registry.gitlab.com,project=group/project,path=/home/user/Dockerfiles/latest_ubuntu`

    :param registry: The registry (i.e.: 'registry.gitlab.com').
    Optional value. gitlab_registry_info function is used if not set.
    :type registry: string
    :param project: The project (i.e.: 'group/project').
    Optional value. gitlab_registry_info function is used if not set.
    :type project: string
    :param path: The path (i.e.: '/home/user/Dockerfiles/latest_ubuntu').
    Default value is '.'.
    :type path: string
    """
    docker_tags = list_docker_tags(path)
    if docker_tags:
        if (registry is "" or project is ""):
            registry, project = gitlab_registry_info()
        if gitlab_registry_login(registry):
            for docker_tag in docker_tags:
                if not gitlab_registry_build(registry, project, docker_tag, path):
                    break
                if not gitlab_registry_push(registry, project, docker_tag):
                    break
